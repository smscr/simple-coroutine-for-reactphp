<?php

namespace Shavshukov\Coroutine;

use function React\Promise\{map, resolve, reject};

/**
 * Get promise that resolves $value with support for Generator coroutines
 *
 * @param mixed $resolvable
 * @param bool $call
 * @return \React\Promise\ExtendedPromiseInterface|\React\Promise\CancellablePromiseInterface
 */
function coroutine($resolvable = null, bool $call = true, \Generator $generator = null)
{
    if ($call && \is_callable($resolvable)) {
        try {
            // resolve a callable recursively
            $result = coroutine($resolvable(), $call);
        } catch (\Throwable $e) {
            $result = reject($e);
        }
    } else if ($resolvable instanceof \Generator) {
        // got a generator at its start (presumably), so we get its current yielded value
        $result = coroutine($resolvable->current(), false, $resolvable);
    } else if (is_array($resolvable)) {
        // resolving an array of values recursively
        $result = map($resolvable, static function ($value) use ($call) {
            return coroutine($value, $call);
        });
    } else if (\is_object($resolvable) && \method_exists($resolvable, 'then')) {
        // resolving a promise/promisable
        $result = resolve($resolvable)->then(static function ($value) use ($call) {
            return coroutine($value, $call);
        });
    } else {
        // resolving a non-array value, not-promise not recursively - the only mean to return something finally
        $result = resolve($resolvable);
    }
    
    // check whether this is a recursive call under a waiting Generator
    // if not, just return the result
    if ($generator === null) {
        return $result;
    }
    
    // Resolve yield of the Generator by Promise, recursively resolve its next yield until Generator returns
    // Return is detected via method Generator::valid, it is assumed that returned Generator is closed
    return $result->then(
        static function ($result) use ($generator, $call) {
            // check if the generator waits at yield statement for result
            if ($generator->valid()) {
                // send the result of the Promise to a waiting yield statement, then process next yield value
                return coroutine($generator->send($result), $call, $generator);
            }

            // the generator does not wait at any yield statement anymore, so we return the final result
            return coroutine($generator->getReturn(), $call);
        },

        static function ($error) use ($generator, $call) {
            // send the result of the thrown error to a waiting yield statement, then process next yield value
            return coroutine($generator->throw($error), $call, $generator);
        }
    );
}
