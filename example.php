<?php

require_once __DIR__ . '/vendor/autoload.php';

use React\Promise;
use React\Promise\PromiseInterface;
use function Shavshukov\Coroutine\coroutine;

function resolvePromises(PromiseInterface $promise1, PromiseInterface $promise2, PromiseInterface $promise3, callable $resultsCallback)
{
    try {
        // instead of Promise\resolve($promise1)->then(function ($result1) {...});
        $result1 = yield $promise1;
        
        // instead of Promise\all([$promise2, $promise3])->then(function ($results23) {...});
        $results23 = yield [
            $promise2,
            $promise3
        ];
        
        $generator = function () use ($result1, $results23) {
            $results = [];
            
            $results[] = yield $result1;
            foreach ($results23 as $result) {
                $results[] = yield $result;
            }
            
            return $results;
        };
        
        // coroutine will call this generator function and resolve it
        $results123 = yield $generator;
        
        // if you pass a \Generator instance it will also resolve it
        $onceMoreResults123 = yield $generator();
        
        // if a PromiseInterface resolves to a \Generator, it will also resolve the \Generator value
        // this would also work with array of promises that resolve to \Generators
        // and this would work recursively
        $secondTimeMoreResults123 = yield Promise\resolve()->then(function () use ($generator) {
             return $generator();
        });
        
        // values will be passed to the $resultsCallback, not promises or \Generator instances
        $finalResult = yield $resultsCallback($secondTimeMoreResults123);
        
        return $finalResult;
    } catch (\Throwable $e) {
        // return an exception if caught
        return $e;
    }
}

// coroutine function accepts promises, values, \Generator instances and arrays of such, and always returns a promise
coroutine(function () {
    $return = yield resolvePromises(Promise\resolve(1), Promise\resolve(2), Promise\resolve(3), function (array $secondTimeMoreResults123) {
        return implode(', ', $secondTimeMoreResults123);
    });
    
    return $return;
})->then(function (string $finalResult1) {
    // will print 1, 2, 3
    echo $finalResult1 . PHP_EOL;
});

coroutine(function () {
    $return = yield resolvePromises(Promise\resolve(1), Promise\resolve(2), Promise\resolve(3), function (array $secondTimeMoreResults123) {
        throw new \Exception('Sometines, something happens...');
    });
    
    return $return;
})->then(function (\Throwable $exception) {
    // will print "Something happened! Sometines, something happens..."
    echo "Something happened! {$exception->getMessage()}" . PHP_EOL;
});