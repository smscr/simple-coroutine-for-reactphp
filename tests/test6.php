<?php

require_once __DIR__ . '/../vendor/autoload.php';

$loop = React\EventLoop\Factory::create();

$sleepForAndThen = function ($seconds, $then) use ($loop) {
    $deferred = new React\Promise\Deferred;

    $loop->addTimer($seconds, function() use ($deferred, $then) {
        Shavshukov\Coroutine\coroutine(function () use ($deferred, $then) {
            $result = yield $then();
            echo $result;
            $deferred->resolve($result);
        });
    });

    return $deferred->promise();
};

Shavshukov\Coroutine\coroutine(function() use ($sleepForAndThen) {

  $pr1 = $sleepForAndThen(2, function() {
    return "First\n";    
  });

  $pr2 = $sleepForAndThen(1, function() {
    return "Second\n";
  });

  return [yield $pr1, yield $pr2];
})->then(function ($result) {
   print_r($result);
   echo "OK\n";
});

$loop->run();
