<?php

require_once __DIR__ . '/../vendor/autoload.php';

$loop = React\EventLoop\Factory::create();

$sleepForAndThen = function ($seconds, $then) use ($loop) {
    $deferred = new React\Promise\Deferred;

    $loop->addTimer($seconds, function() use ($deferred, $then) {
        Shavshukov\Coroutine\coroutine(function () use ($deferred, $then) {
            try {
               $result = yield $then();
               echo $result;
               $deferred->resolve($result);
            } catch (Exception $e) {
               echo "Exception: {$e->getMessage()}\n";

               throw $e;
            }
        })->otherwise(function ($e) use ($deferred) {
            $deferred->reject($e);
        });
    });

    return $deferred->promise();
};

Shavshukov\Coroutine\coroutine(function() use ($sleepForAndThen) {

  $pr1 = $sleepForAndThen(2, function($yes = false) {
    $g = function () {
        return yield "First\n";
    };

    return yield $g();
  });

  $pr2 = $sleepForAndThen(1, function($yes = false) {
    $g = function () {
       $a = yield "Second\n";
       return new Exception($a);
    };

    throw yield $g();
  });

  return [yield $pr1, yield $pr2];
})->then(function ($result) {
   print_r($result);
   echo "OK\n";
}, function ($e) {
   print_r([
     'class' => get_class($e),
     'code' => $e->getCode(),
     'message' => $e->getMessage()
   ]);
   echo "ERROR\n";
});

$loop->run();
