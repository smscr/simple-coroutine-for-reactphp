<?php

require_once __DIR__ . '/../vendor/autoload.php';

$loop = React\EventLoop\Factory::create();

$sleepForAndThen = function ($seconds, $then) use ($loop) {
    $deferred = new React\Promise\Deferred;

    $loop->addTimer($seconds, function() use ($deferred, $then) {
        Shavshukov\Coroutine\coroutine(function () use ($deferred, $then) {
            try {
               $result = yield $then();
               echo $result;
               $deferred->resolve($result);
            } catch (Exception $e) {
               echo "Exception: {$e->getMessage()}";

               try {
                  $result = yield $then(true);
                  echo $result;
                  $deferred->resolve($result);
               } catch (Exception $e2) {
                  echo "Exception: {$e2->getMessage()}";
                  $deferred->reject($e2);
               }
            }
        });
    });

    return $deferred->promise();
};

Shavshukov\Coroutine\coroutine(function() use ($sleepForAndThen) {
  return yield [
    $sleepForAndThen(2, function($yes = false) {
       return "First\n";    
    }),
    
    $sleepForAndThen(1, function($yes = false) {
       if ($yes) return "Second with Yes\n";
       throw new Exception("Second\n");
    })
  ];
})->then(function ($result) {
   print_r($result);
   echo "OK\n";
}, function ($e) {
   print_r([
     'class' => get_class($e),
     'code' => $e->getCode(),
     'message' => $e->getMessage()
   ]);
   echo "ERROR\n";
});

$loop->run();
